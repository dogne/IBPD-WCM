package com.ibpd.henuocms.service.user;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.UserEntity;

public interface IUserService extends IBaseService<UserEntity> {

	UserEntity getUserByUserName(String userName);

	boolean checkUserExist(String userName);
}
 