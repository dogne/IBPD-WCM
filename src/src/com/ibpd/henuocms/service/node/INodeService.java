package com.ibpd.henuocms.service.node;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.ext.NodeExtEntity;

public interface INodeService extends IBaseService<NodeEntity> {
	void initNodeDemo();
	List<NodeExtEntity> getChildNodeList(Long siteId,Long nodeId,Integer pageSize,Integer pageIndex,String orderField,String orderType);
	List<NodeEntity> getChildNodeList(Long nodeId);
	NodeExtEntity getNodeExtEntity(Long nodeId);
}
 