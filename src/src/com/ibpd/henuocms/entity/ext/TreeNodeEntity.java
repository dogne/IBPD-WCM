package com.ibpd.henuocms.entity.ext;

import java.util.Date;

/**
 * 专为jquery easyui定制的tree类
 * @author 马根
 *
 */
public class TreeNodeEntity {
	
	private String id;
	private String text;
	private String state;//close and open 代表是否有下级
	private String iconCls;
	private String checked="false";
	
	public TreeNodeEntity(){
		super();
	}
	public TreeNodeEntity(String id,String text,String state,String iconCls,String checked){
		this.id=id;
		this.text=text;
		this.state=state;
		this.iconCls=iconCls;
		this.checked=checked;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}
	public String getIconCls() {
		return iconCls;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	public String getChecked() {
		return checked;
	}
	
}
