<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
    <div id="nodeview" region="center" style="background: #eee; overflow-y:hidden;overflow-x:auto;">
		<div style="width: 1500px;height:100%;">
		<table id="grid" style="height:auto;" title="订单信息" iconcls="icon-view">            
        </table>
		</div>
   </div>
   <div region="east" hide="true" split="false" title="" style="width:500px;" id="west">
	    <iframe id="propsPanel" onreadystatechange="resize()"  src="<%=path %>/Manage/Order/details.do?id=-1" scrolling="auto"  frameborder="0" style="width:100%;height:100%;"></iframe>
    </div>
	</div>

	
	<script type="text/javascript">
	$(document).ready(function(){
		InitGrid();//默认没有传参数,参数应该是querydata
	});
	 var oTime = null;
    function resize()
    {
        if(oTime)
        {
            clearTimeout(oTime);
        }
         
        oTime = setTimeout(reset, 200);
    }
     
    function reset()
    {
        var frame = document.getElementById("propsPanel");
        var outHeight = frame.offsetHeight;
        var inHeight = frame.contentWindow.document.body.scrollHeight;
        if(outHeight != inHeight)
        {
            frame.style.height = (inHeight + 10) + "px";
        }
    }

	function hiddenToNav(){
		var _setNode=$('#grid').datagrid("getSelected");
		$.post(
					basePath+"/Manage/Catalog/navi.do",
					{id:_setNode.id},
					function(result){
						msgShow("提示","执行成功.","warning");
						reload();
					}
				);

	};
	//not used function
	function closeNode(){
		var _setNode=$('#grid').datagrid("getSelected");
		$.post(
					basePath+"/Manage/Catalog/state.do",
					{id:_setNode.id},
					function(result){
						//msgShow("提示","锁定成功.","warning");
						$('#grid').datagrid("reload");
					}
				);
	};
	function reload(){
		$('#grid').datagrid("reload");
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	var cmenu;
    function createColumnMenu(){
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#grid').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#grid').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#grid').datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#grid').datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
      function InitGrid(queryData) {
            $('#grid').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>Manage/Order/list.do?status=${status }&paystatus=${paystatus }&t='+new Date(), 
                title: '订单列表',
                iconCls: 'icon-ok',
                singleSelect:false,
                fit:true,
               // width: 1500,//function () { return document.body.clientWidth * 0.98 },
				//height:function () { return $("#nodeview").height() },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                },
                onRowContextMenu:function(e, rowIndex, rowData){
        			
   				},
   				rowStyler:function(index,row){
   					//if(row.state<=0){
   					//return "background-color:RGB(79,129,189);";
   					//}else{
   					//}
   				},
                columns: [[
                    { field: 'ck', checkbox: true,title:'选择' },   //选择
					{ title: 'ID', field: 'id', width: 40, sortable:true,hidden:true },
					{ title: '用户名称', field: 'account', width: 40, sortable:true},
					{ title: '创建日期', field: 'createdate', width: 40, sortable:true,formatter:function(val, rowdata, index){return 1900+val.year+"-"+(1+val.month)+"-"+val.date}},
					{ title: '折扣', field: 'rebate', width: 40, sortable:true},
					{ title: '状态', field: 'status', width: 40, sortable:true},
					{ title: '商品数量', field: 'quantity', width: 40, sortable:true},
					{ title: '商品总额', field: 'ptotal', width: 40, sortable:true},
					{ title: '支付总额', field: 'amount', width: 40, sortable:true,formatter:function(val, rowdata, index){if(rowdata.updateAmount=='y'){return val+"<span style='color:red;font-size:10px'>[修]</span>"}else{return val;}}},
					{ title: '更新金额', field: 'updateAmount', width: 40, sortable:true},
					{ title: '支付方式', field: 'payType', width: 40, sortable:true},
					{ title: '付款状态', field: 'paystatus', width: 40, sortable:true},
					{ title: '退款状态', field: 'refundStatus', width: 40, sortable:true},
					{ title: '运费', field: 'fee', width: 40, sortable:true},
					{ title: '物流编码', field: 'expressCode', width: 40, sortable:true},
					{ title: '快递公司', field: 'expressCompanyName', width: 40, sortable:true},
					{ title: '快递名称', field: 'expressName', width: 40, sortable:true},
					{ title: '发货备注', field: 'otherRequirement', width: 40, sortable:true},
					{ title: '快递单号', field: 'expressNo', width: 40, sortable:true},
					{ title: '关闭说明', field: 'closedComment', width: 40, sortable:true},
					{ title: '送积分', field: 'score', width: 40, sortable:true},
					{ title: '备注', field: 'remark', width: 40, sortable:true}
               ]], 
                toolbar: [
				<c:if test="${status=='init'}">
				{
                    id: 'btnPass',
                    text: '审核',
                    iconCls: 'icon-add',
                    handler: function () {
                        pass();
                    }
                },
				</c:if>
				<c:if test="${status=='pass' && paystatus=='y'}">
				{
                    id: 'btnSend',
                    text: '发货',
                    iconCls: 'icon-edit',
                    handler: function () {
                        send();
                    }
                },
				</c:if>
				<c:if test="${status=='sign'}">
				{
                    id: 'btnFile',
                    text: '归档',
                    iconCls: 'icon-ok',
                    handler: function () {
                        file();
                    }
                },
				</c:if>
				<c:if test="${(status=='pass' || status=='init') && paystatus=='n'}">
				{
                    id: 'btnCancel',
                    text: '取消',
                    iconCls: 'icon-remove',
                    handler: function () {
                        cancel();
                    }
                }, 
				</c:if>
				<c:if test="${(status=='pass' || status=='init') && paystatus=='n'}">
				{
                    id: 'btnPrice',
                    text: '改价',
                    iconCls: 'icon-edit',
                    handler: function () {
                        updatePrice();
                    }
                }, 
				</c:if>
				'-', {
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                },{
                    id: 'btnPrint',
                    text: '打印',
                    iconCls: 'icon-print',
                    handler: function () {
                        print();
                    }
                }],
                onDblClickRow: function (rowIndex, rowData) {
                	
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                },
                onClickRow: function (rowIndex, rowData) {
                	loadDetails(rowData.id);
                    //$('#grid').datagrid('uncheckAll');
                    //$('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                }
            })
        };
		function getGridSelections(){
			var _sets=$('#grid').datagrid("getSelections");
			if(typeof(_sets)=="undefined"){
				//msgShow("提示","没有选中行","warning");
				return "";
			}else{
				var _ids="";
				$.each(_sets,function(i,n){
				_ids+=n.id+",";
				});
				return _ids;
			}
			return "";
		};
		function changeStatus(status){
			var ids=getGridSelections();
			if(ids.length==0){
				msgShow("错误","没有选中行","warning");
				return;
			}
			$.post(
				basePath+"/Manage/Order/doPass.do",
				{ids:ids,status:status},
				function(result){
					reload();
					
				}
			);
		};
        function pass(){
			changeStatus("pass");
		};
		function send(){
			ShowExpCodeSaveDialog();
		};
		function file(){
			changeStatus("file");
		};
		function cancel(){
			ShowCancelOrderDialog();
		};
		function print(){
		};
		function updatePrice(){
			ShowUpdatePriceDialog();
		};
		function msgShow(title, msgString, msgType) {
			$.messager.alert(title, msgString, msgType);
		};
        function loadDetails(id){
			$("#propsPanel").attr("src","<%=path %>/Manage/Order/details.do?id="+id+"&t="+new Date());
        };
        function ShowExpCodeSaveDialog(){
			var nodeId="-1";
			var node=$('#grid').datagrid("getSelected");
			nodeId=node.id;
			var addDialog = $('<div id="addNodeDiv"/>').appendTo('body');
			
			$(addDialog).dialog({
				modal:true,
				title:'输入快递单号',
				shadow:true,
				iconCls:'icon-add',
				width:400,
				height:300,
				resizable:true,
				toolbar:[{
						text:'保存',
						iconCls:'icon-save',
						handler:function(){
							if($("#addiframe")[0].contentWindow.submit()){
								$(addDialog).dialog("close");
								$(addDialog).remove();
								changeStatus("send");
							}else{
								msgShow("err","失败","error");
							}
						}
					},'-',{
						text:'取消',
						iconCls:'icon-cancel',
						handler:function(){
							$(addDialog).dialog("close");
							$(addDialog).remove();
						}
					}],
				content:'<iframe id="addiframe" width="385px" height="255px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/Order/expCode.do?id='+nodeId+'&t='+new Date()+'"></iframe>'
			});
			$(addDialog).dialog("open");
        };
       function ShowCancelOrderDialog(){
			nodeId=getGridSelections();;
			if(nodeId.length==0){
				msgShow("错误","没有选中行","warning");
				return;
			}
			var addDialog = $('<div id="addNodeDiv"/>').appendTo('body');
			
			$(addDialog).dialog({
				modal:true,
				title:'取消订单',
				shadow:true,
				iconCls:'icon-add',
				width:400,
				height:300,
				resizable:true,
				toolbar:[{
						text:'保存',
						iconCls:'icon-save',
						handler:function(){
							if($("#addiframe")[0].contentWindow.submit()){
								$(addDialog).dialog("close");
								$(addDialog).remove();
								reload();
							}else{
								msgShow("err","失败","error");
							}
						}
					},'-',{
						text:'取消',
						iconCls:'icon-cancel',
						handler:function(){
							$(addDialog).dialog("close");
							$(addDialog).remove();
						}
					}],
				content:'<iframe id="addiframe" width="385px" height="255px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/Order/cancelOrder.do?ids='+nodeId+'&t='+new Date()+'"></iframe>'
			});
			$(addDialog).dialog("open");
        };
       function ShowUpdatePriceDialog(){
			nodeId=getGridSelections();
			if(nodeId.length==0){
				msgShow("错误","没有选中行","warning");
				return;
			}
			
			var addDialog = $('<div id="addNodeDiv"/>').appendTo('body');
			
			$(addDialog).dialog({
				modal:true,
				title:'改价',
				shadow:true,
				iconCls:'icon-add',
				width:400,
				height:300,
				resizable:true,
				toolbar:[{
						text:'保存',
						iconCls:'icon-save',
						handler:function(){
							if($("#addiframe")[0].contentWindow.submit()){
								$(addDialog).dialog("close");
								$(addDialog).remove();
								reload();
							}else{
								msgShow("err","失败","error");
							}
						}
					},'-',{
						text:'取消',
						iconCls:'icon-cancel',
						handler:function(){
							$(addDialog).dialog("close");
							$(addDialog).remove();
						}
					}],
				content:'<iframe id="addiframe" width="385px" height="255px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/Order/updatePrice.do?ids='+nodeId+'&t='+new Date()+'"></iframe>'
			});
			$(addDialog).dialog("open");
        };
		function print(){
			nodeId=getGridSelections();
			if(nodeId.length==0){
				msgShow("错误","没有选中行","warning");
				return;
			}
			window.open(basePath+'/Manage/Order/print.do?ids='+nodeId+'&t='+new Date(),'订单打印','toolbar=no,menubar=no,resizable=no,location=no,status=no,z-look:yes');
		};
     </script>
	</body>
</html>
