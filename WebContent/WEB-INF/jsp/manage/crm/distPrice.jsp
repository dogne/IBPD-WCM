<%@ page language="java" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort() + path + "/";

	StringBuffer uploadUrl = new StringBuffer("http://");
	uploadUrl.append(request.getHeader("Host"));
	uploadUrl.append(request.getContextPath());
	uploadUrl.append("/manage/crm/doUploadByBaseBarCode.do");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		
		<title>获取商品配送加价</title>
   <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	 <style type="text/css">
	 #tb{
		width:800px;
		border:solid 1px black;
	 }
	 #tb tr th{
		font-weight:bold;
		font-size:12px;
		backgrouund-color:#404040;
		border:solid 1px black;
	 }
	 #tb tr td{
		border:solid 1px black;
		text-align:center;
	 }
	 
	 </style>
		<script type="text/javascript">
		$(document).ready(function(){
		$("#content").html("");
			$("#smt").click(function(){post()});
		});
		function post(){ 
			$.post(
					"<%=basePath %>manage/crm/getDistPrice.do",
					{goodsCode:$("#goodsCodes").val()},
					function(result){   
						if(result.indexOf("status")!=-1){
						result=("["+result+"]");
						}
						var r=eval(result);
						if(r[0].msg){
							alert(r[0].msg);
						}else{ 
							$("<table id='tb' cellpadding='0' cellspacing='0'></table>").appendTo("#content");
							$("#tb").html("<tr><th>商品编码</th><th>部门编码</th><th>定价方式</th><th>配送价格</th></tr>");
							for(i=0;i<r.length;i++){  
								var tr="<tr>";   
								tr+="<td>"+r[i].GoodsCode+"</td>";
								tr+="<td>"+r[i].DeptCode+"</td>";
								tr+="<td>"+r[i].PriceMode+"</td>";
								tr+="<td>"+r[i].DistPrice+"</td>";
								tr+="</tr>";    
								$("#tb").html($("#tb").html()+tr);
							}
						}
						
					}
				);
		}
		</script>
	</head>
	<body style="background-color: #C0D1E3; padding: 2px;">
		<div id="form" style="float:left;margin:0;padding:0;width:100%;margin-top:20px;">
			输入商品编码(以tab分割):<br/><input type="text" id="goodsCodes"/><br/>
			<input type="button" value="提交" id="smt"/>
		</div>
		<div id="content" style="float:left;margin:0;padding:0;width:100%;margin-top:20px;">
			
		</div>
	
	</body>
</html>