<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
    <form id="fm" class="forms" action="<%=basePath%>manage/template/doAdd.do" method="post">
    	<input type="hidden" name="id" id="id" value=""/>
    	<table cellpadding="0" cellspacing="0" border="0">
    		<tr>
    			<th class="title">模板名称</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="templateName" name="templateName"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">模板类型</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="templateType" name="templateType"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">分组</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="group" name="group"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">排序</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="order" name="order"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
     	</table>
    	<input type="submit"/>&nbsp;&nbsp;<input type="button" value="关闭" onclick="javascript:$('#addDialog').window('close');"/>
    </form> 
    <script type="text/javascript">
    $(function(){
		
	$("#fm").Validform({
		tiptype:2,
		callback:function(form){
			var check=confirm("您确定要提交表单吗？");
			if(check){
				form[0].submit();
			}
			
			return false;
		}
		
	});
})
</script>